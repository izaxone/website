# Portfolio Site Generator
[![Netlify Status](https://api.netlify.com/api/v1/badges/4079d220-ad54-46aa-b239-46bc887f1a13/deploy-status)](https://app.netlify.com/sites/glittering-semolina-deb486/deploys)
This is the code of my portfolio site. It features a fancy cover page, as well as an about page in a resume style that is generated from raw JSON data. It can also generate a PDF from this data. This data can either come from a hardcoded `.json` file, or from an API. 

This app is written in Vue 3 with JavaScript. 
## Project setup
```
yarn
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
